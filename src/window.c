/* window.c
 *
 * Copyright 2020 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "window.h"


#include <adwaita.h>
#include <glib/gi18n-lib.h>


struct _DsnTypography {
  GtkApplicationWindow parent_instance;
};


G_DEFINE_TYPE (DsnTypography, dsn_typography, GTK_TYPE_APPLICATION_WINDOW)


static void
dsn_typography_class_init (DsnTypographyClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/design/Typography/window.ui");
}


static void
copy_activated (GSimpleAction *action,
                GVariant      *parameter,
                gpointer       data)
{
  GdkDisplay *display = gtk_widget_get_display (GTK_WIDGET (data));
  GdkClipboard *clipboard = gdk_display_get_clipboard (display);

  gdk_clipboard_set_text (clipboard, g_variant_get_string (parameter, NULL));
}

static void
uri_launched (GtkUriLauncher *launcher,
              GAsyncResult   *result)
{
  g_autoptr (GError) error = NULL;

  if (!gtk_uri_launcher_launch_finish (launcher, result, &error))
    g_warning ("Couldn't open uri: %s\n", error->message);
}

static void
guidelines_activated (GSimpleAction *action,
                      GVariant      *parameter,
                      gpointer       data)
{
  GtkWindow *window = GTK_WINDOW (data);
  GtkUriLauncher *launcher;

  launcher = gtk_uri_launcher_new (
    "https://developer.gnome.org/hig/guidelines/typography.html");
  gtk_uri_launcher_launch (launcher, window, NULL,
                           (GAsyncReadyCallback) uri_launched, NULL);
}


static void
about_activated (GSimpleAction *action,
                 GVariant      *parameter,
                 gpointer       data)
{
  GtkWidget *parent = GTK_WIDGET (data);
  const char *developers[] = { "Zander Brown <zbrown@gnome.org>", NULL };
  const char *designers[] = { "Tobias Bernard", NULL };
  g_autofree char *copyright = NULL;

  copyright = g_strdup_printf (_("Copyright © %s Zander Brown"),
                               "2020");

  adw_show_about_dialog (parent,
                         "application-icon", "org.gnome.design.Typography",
                         "application-name", _("Typography"),
                         "developers", developers,
                         "designers", designers,
                         "website", "https://apps.gnome.org/app/org.gnome.design.Typography/",
                         "issue-url", "https://gitlab.gnome.org/World/design/typography/-/issues/new",
                         // Translators: Credit yourself here
                         "translator-credits", _("translator-credits"),
                         "copyright", copyright,
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "version", PACKAGE_VERSION,
                         NULL);
}


static const GActionEntry entries[] = {
  { "copy", copy_activated, "s", NULL, NULL },
  { "guidelines", guidelines_activated, NULL, NULL, NULL },
  { "about", about_activated, NULL, NULL, NULL },
};


static void
dsn_typography_init (DsnTypography *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   entries,
                                   G_N_ELEMENTS (entries),
                                   self);

}

