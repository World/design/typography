/* main.c
 *
 * Copyright 2020 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <adwaita.h>

#include "config.h"
#include "window.h"


static void
activate (GtkApplication *app)
{
  GtkWindow *window;

  window = gtk_application_get_active_window (app);
  if (window == NULL) {
    window = g_object_new (DSN_TYPE_TYPOGRAPHY,
                           "application", app,
                           NULL);
  }

  gtk_window_present (window);
}


int
main (int   argc,
      char *argv[])
{
  g_autoptr (AdwApplication) app = NULL;

  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  app = adw_application_new ("org.gnome.design.Typography", G_APPLICATION_DEFAULT_FLAGS);

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}

