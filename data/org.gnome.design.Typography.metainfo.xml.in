<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2019 Zander Brown -->
<component type="desktop-application">
  <id>org.gnome.design.Typography</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0</project_license>
  <project_group>GNOME</project_group>
  <name>Typography</name>
  <summary>Look up text styles</summary>
  <description>
    <p>
      Tool for working with the GNOME typography design guidelines.
    </p>
  </description>
  <kudos>
    <kudo>HiDpiIcon</kudo>
    <kudo>ModernToolkit</kudo>
  </kudos>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/design/typography/raw/HEAD/data/screenshots/screenshot1.png</image>
    </screenshot>
  </screenshots>
  <launchable type="desktop-id">org.gnome.design.Typography.desktop</launchable>
  <url type="homepage">https://gitlab.gnome.org/World/Design/Typography</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/Design/Typography/issues</url>
  <update_contact>zbrown@gnome.org</update_contact>
  <translation type="gettext">org.gnome.design.Typography</translation>
  <content_rating type="oars-1.1" />
  <!-- Translators: This is my name -->
  <developer_name>Zander Brown</developer_name>
  <custom>
    <value key="GnomeSoftware::key-colors">[(176, 182, 188)]</value>
  </custom>
  <releases>
    <release version="0.3.0" date="2024-03-20">
      <description>
        <p>Typography now uses latest widgets from libadwaita. Added
        translations to multiple languages.</p>
      </description>
    </release>
    <release version="0.2.0" date="2022-02-01">
      <description>
        <p>Adds support for dark mode and remove the large-title class.</p>
      </description>
    </release>
    <release version="0.1.0" date="2020-12-17"/>
  </releases>
</component>
