<a href="https://flathub.org/apps/details/org.gnome.design.Typography">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Typography

<img src="https://gitlab.gnome.org/World/design/typography/raw/master/data/icons/hicolor/scalable/apps/org.gnome.design.Typography.svg" width="128" height="128" />
<p>Typography tool.</p>

## Screenshots

<div align="center">
![screenshot](data/screenshots/screenshot1.png)
</div>

## Hack on Typography

To build the development version of Typography and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow the GNOME Code of Conduct when participating in project
spaces.



